## What are we talking about today?

- What is module development? <!-- .element: class="fragment" -->
- From info to YAML <!-- .element: class="fragment" -->
- What happened to hook_menu? <!-- .element: class="fragment" -->
- Composer, PSR-4, namespaces and autoloading <!-- .element: class="fragment" -->
- Services and the dependency injection container <!-- .element: class="fragment" -->
- My first controller <!-- .element: class="fragment" -->
- My first Twig template <!-- .element: class="fragment" -->

Notes:

- Talk about only scratching the surface today.
- I learn by doing and haven't had as much time to learn as I'd like.
- I welcome helpful input.
